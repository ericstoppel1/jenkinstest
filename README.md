Kubernetes / Minikubes / docker 

How to set up minikubes to run kubernetes clusters locally:

Install kubectl:
	1) Download the latest release with the command:
		-> curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
	2) Make the kubectl binary executable.
		-> chmod +x ./kubectl
	3) the binary in to your PATH.
		-> sudo mv ./kubectl /usr/local/bin/kubectl
	4) Check if everything is ok
		-> kubectl version

Install Minikube
	. There are two ways to install minikube, installing minikube either in a virtual machine or in docker, we are going to choose the second option, docker.

1) Install docker if you have not done it yet:
	https://docs.docker.com/install/linux/docker-ce/ubuntu/

2) Install minikube on linux
	-> curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube
3) Get your minikube command available on you pc
	-> sudo cp minikube /usr/local/bin && rm minikube


Starting Minikube:
	-> sudo minkube start --vm-driver=none (the option --vm-driver=none makes minikube start in docker containers)

you should see all the containers used to run minikubes active, just execute docker ps.

Now we are ready to interact with our cluster, lets deploy a composed docker application:

if you want to deploy simple docker images use this link:
https://kubernetes.io/docs/setup/learning-environment/minikube/

We need to install kompose in order to deploy a composed application, kompose = kubernetes + docker-compose

# Linux 
	-> curl -L https://github.com/kubernetes-incubator/kompose/releases/download/v0.7.0/kompose-linux-amd64 -o kompose
	-> chmod +x kompose 
	-> sudo mv ./kompose /usr/local/bin/kompose

IMPORTANT: we have to tag our images with other tan latest for this to work.
IMPORTANT 2: kompose only supports versions 1,2 or 3 of our docker-compose.yml, not 2.3, just 2.

1) Go to your docker-compose folder
2) -> kompose up

we can simply execute kompose up and it will convert our files to a kubernetes understanding and deploy them all at once.

if not, we can execute: kompose convert and then kubectl apply -f <output file>

now lets verify our app was succesfully deployed to kubernetes
 -> kubectl get deployment,svc,pods

 you should see your deployments, services and pods running.

To shut down your cluster just enter cmd and execute: kompose down

SETTING DE UI DASHBOARD:

1)Deploy de dashboard
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta1/aio/deploy/recommended.yaml
2) create a user for your dashboard
	Follow this guide: https://github.com/kubernetes/dashboard/wiki/Creating-sample-user
3) Launch the dashboard
	kubectl proxy
4) Enter the dashboard at 
	http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/.
