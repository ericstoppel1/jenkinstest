
const express = require('express');
const Pool = require('pg').Pool;
const server = express();
const cors = require('cors');
server.use(cors());
server.use(express.json());
var path = require('path');


const pool = new Pool({
  user: 'docker',
  host: 'db',
  database: 'docker',
  password: 'docker',
  port: 5432,
})

const port = 3003;
const base_api = "/";

server.listen(port, function () {
  console.log(`Corriendo api ${port}...`);
});

server.get(`${base_api}getHello`, function(req, res) {
  pool.query('SELECT * from web_origins', (error, results) => {
    if (error) {
      throw error
    }
    res.status(200).json(results.rows)
  })
});

server.get(`${base_api}`, function(req, res) {
  console.log(path.join(__dirname + '/simplePage.html'));
  res.sendFile(path.join(__dirname + '/simplePage.html'));
});

